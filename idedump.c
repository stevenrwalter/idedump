#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <conio.h>
#include <i86.h>

#define PRIMARY_BASE (0x1f0)
#define SECONDARY_BASE (0x170)

#define BASE_ADDR SECONDARY_BASE

#define DATA_REG 	(BASE_ADDR + 0)
#define ERROR_REG 	(BASE_ADDR + 1)
// Alias of ERROR_REG
#define PRECOMP_REG 	(BASE_ADDR + 1)
#define SEC_COUNT_REG 	(BASE_ADDR + 2)
#define START_SEC_REG 	(BASE_ADDR + 3)
#define LOW_CYL_REG 	(BASE_ADDR + 4)
#define HIGH_CYL_REG 	(BASE_ADDR + 5)
#define DRIVE_HEAD_REG 	(BASE_ADDR + 6)
#define CMD_REG 	(BASE_ADDR + 7)
// Alias of CMD_REG
#define STATUS_REG 	(BASE_ADDR + 7)

#define CMD_RESTORE 	(0x16)
#define CMD_SEEK	(0x36)
// Data only (no ECC), with retries
#define CMD_READ	(0x20)
// No retries
#define CMD_READ_VERIFY (0x41)
#define CMD_SET_PARAMS  (0x91)

#define STATUS_BUSY	(1 << 7)
#define STATUS_READY	(1 << 6)
#define STATUS_SEEK_DONE (1 << 4)
#define STATUS_DRQ 	(1 << 3)
#define STATUS_ERROR 	(1 << 0)

#define BAD_BLOCK_ERROR	(1 << 7)
#define DATA_ECC_ERROR  (1 << 6)
#define ID_MISSING_ERROR (1 << 4)
#define DAM_ERROR	(1 << 0)

void wait_not_busy(void)
{
    printf("Waiting for idle...");
    delay(1);
    while (inp(STATUS_REG) & STATUS_BUSY);
    delay(1);
    printf("IDLE!\n");
}

void wait_ready(void)
{
    printf("Waiting for ready...");
    while (!(inp(STATUS_REG) & STATUS_READY));
    printf("READY!\n");
}

void wait_seek_done(void)
{
    printf("Waiting for ready...");
    while (!(inp(STATUS_REG) & STATUS_SEEK_DONE));
    printf("READY!\n");
}

void wait_data_ready(void)
{
    //printf("Waiting for data ready...");
    while (!(inp(STATUS_REG) & STATUS_DRQ));
    //printf("DRQ!\n");
}

int set_params(int num_cyls, int num_heads, int num_sectors, int precomp)
{
    wait_not_busy();
    outp(PRECOMP_REG, precomp >> 2);
    outp(SEC_COUNT_REG, num_sectors);
    outp(LOW_CYL_REG, num_cyls & 0xff);
    outp(HIGH_CYL_REG, num_cyls >> 8);
    outp(DRIVE_HEAD_REG, 0xa0 | (num_heads-1));

    wait_ready();
    outp(CMD_REG, CMD_SET_PARAMS);
    wait_not_busy();
    if (inp(STATUS_REG) & STATUS_ERROR) {
	fprintf(stderr, "Error during SET_PARAMS: %x\n", inp(ERROR_REG));
	return 0;
    }

    return 1;
}

int read_verify(int cyl, int head, int sector)
{
    wait_not_busy();
    outp(SEC_COUNT_REG, 1);
    outp(START_SEC_REG, sector);
    outp(LOW_CYL_REG, cyl & 0xff);
    outp(HIGH_CYL_REG, cyl >> 8);
    outp(DRIVE_HEAD_REG, 0xa0 | head);

    wait_ready();
    outp(CMD_REG, CMD_READ_VERIFY);
    wait_not_busy();
    if (inp(STATUS_REG) & STATUS_ERROR) {
	fprintf(stderr, "Error during READ_VERIFY: %x\n", inp(ERROR_REG));
	return 0;
    }

    return 1;
}

static short buffer[512/sizeof(short)];

static int ecc_errors = 0;
static int missing_sectors = 0;
static int dam_errors = 0;
static int bad_blocks = 0;

int read_data(int cyl, int head, int sector)
{
    int i;

    wait_not_busy();
    wait_ready();
    _disable();
    outp(SEC_COUNT_REG, 1);
    outp(START_SEC_REG, sector);
    outp(LOW_CYL_REG, cyl & 0xff);
    outp(HIGH_CYL_REG, cyl >> 8);
    outp(DRIVE_HEAD_REG, 0xa0 | head);

    outp(CMD_REG, CMD_READ);
    wait_not_busy();
    if (inp(STATUS_REG) & STATUS_ERROR) {
	int error = inp(ERROR_REG);
	_enable();
	fprintf(stderr, "Error during READ: %x\n", error);
	if (error & DATA_ECC_ERROR) {
	    ecc_errors++;
	} else if (error & ID_MISSING_ERROR) {
	    missing_sectors++;
	} else if (error & DAM_ERROR) {
	    dam_errors++;
	} else if (error & BAD_BLOCK_ERROR) {
	    bad_blocks++;
	} else if (error) {
	    return 0;
	}
	memset(buffer, 0, sizeof(buffer));
    } else {
	_enable();
    }

    for (i=0; i<256; i++) {
	wait_data_ready();
	buffer[i] = (short)inpw(DATA_REG);
    }

    if (inp(STATUS_REG) & STATUS_DRQ) {
	fprintf(stderr, "Unexpected extra data!\n");
	return 0;
    }

    return 1;
}

int seek(int cyl)
{
    wait_not_busy();
    outp(LOW_CYL_REG, cyl & 0xff);
    outp(HIGH_CYL_REG, cyl >> 8);
    outp(DRIVE_HEAD_REG, 0xa0);

    wait_ready();
    outp(CMD_REG, CMD_SEEK);
    wait_seek_done();
    if (inp(STATUS_REG) & STATUS_ERROR) {
	fprintf(stderr, "Error during SEEK: %x\n", inp(ERROR_REG));
	return 0;
    }

    return 1;
}

int restore (void)
{
    wait_not_busy();
    outp(DRIVE_HEAD_REG, 0xa0);

    wait_ready();
    outp(CMD_REG, CMD_RESTORE);
    wait_not_busy();
    if (inp(STATUS_REG) & STATUS_ERROR) {
	fprintf(stderr, "Error during RESTORE: %x\n", inp(ERROR_REG));
	return 0;
    }

    return 1;
}

#define NUM_CYLS 306
#define NUM_HEADS 4
#define NUM_SECTORS 17
#define WRITE_PRECOMP 128

int main(void)
{
    FILE *out;
    int count;
    int cyl, head, sector;

    printf("SRW hello\n");
    if (!restore())
    	return 1;
    if (!set_params(NUM_CYLS, NUM_HEADS, NUM_SECTORS, WRITE_PRECOMP))
    	return 1;
    if (!read_verify(0, 0, 1))
    	return 1;
    if (!read_data(0, 3, 17))
    	return 1;

    out = fopen("image.bin", "w");
    if (!out) {
	perror("fopen");
	return 1;
    }

    for (cyl=0; cyl<NUM_CYLS; cyl++) {
	for (head=0; head<NUM_HEADS; head++) {
	    for (sector=1; sector<=NUM_SECTORS; sector++) {
		if (!read_data(cyl, head, sector))
		    return 1;
		printf("C/H/S %d/%d/%d\n", cyl, head, sector);
		count = fwrite(buffer, sizeof(buffer), 1, out);
		assert(count == 1);
                fflush(out);
	    }
	}
    }

    fclose(out);

    printf("Success, %d errors, %d missing, %d bad blocks, %d DAM\n", ecc_errors, missing_sectors, bad_blocks, dam_errors);

    // Park heads
    seek(656/2 - 1);
    return 0;
}
